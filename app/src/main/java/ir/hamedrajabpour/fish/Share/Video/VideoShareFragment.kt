package ir.hamedrajabpour.fish.Share.Video

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.otaliastudios.cameraview.CameraListener
import com.wonderkiln.camerakit.*
import ir.hamedrajabpour.fish.R
import kotlinx.android.synthetic.main.video_share_fragment.*


class VideoShareFragment : Fragment() {

    companion object {
        fun newInstance() = VideoShareFragment()
    }

    private lateinit var viewModel: VideoShareViewModel
    private var isRecording = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.video_share_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VideoShareViewModel::class.java)
        // TODO: Use the ViewModel
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpCameraView()
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onResume() {
        ShareVideo_Camera.start()
        super.onResume()
    }

    override fun onPause() {
        ShareVideo_Camera.stop()
        super.onPause()
    }

    private fun setUpCameraView() {
        ShareVideo_Camera.addCameraKitListener(object : CameraListener(), CameraKitEventListener {
            override fun onVideo(p0: CameraKitVideo?) {}

            override fun onEvent(p0: CameraKitEvent?) {}

            override fun onError(p0: CameraKitError?) {}

            override fun onImage(p0: CameraKitImage?) {
//                val jpeg = p0?.jpeg
//                val result = BitmapFactory.decodeByteArray(jpeg, 0,jpeg!!.size)
//                saveToInternalStorage(result)
            }
        })


        ShareGallery_Video_Button.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view:View) {
                if (isRecording)
                {
                    ShareVideo_Camera.stopVideo()
                    isRecording = false
                    return
                }

                ShareVideo_Camera.captureVideo()
                isRecording = true
            }
        })



    }


}

