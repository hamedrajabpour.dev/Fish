package ir.hamedrajabpour.fish.Share

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.hamedrajabpour.fish.R
import kotlinx.android.synthetic.main.bottom_sheet_layout.*

class BottomSheetDialog: BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v =inflater.inflate(R.layout.bottom_sheet_layout,container,false)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_gallery.setOnClickListener {
            findNavController().navigate(R.id.galleryShareFragment)
            this.dismiss()

        }

        btn_camera.setOnClickListener {
            findNavController().navigate(R.id.cameraShareFragment)
this.dismiss()
        }

        btn_video.setOnClickListener {
            findNavController().navigate(R.id.videoShareFragment)
            this.dismiss()
        }
    }

}