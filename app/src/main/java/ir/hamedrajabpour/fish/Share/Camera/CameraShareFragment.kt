package ir.hamedrajabpour.fish.Share.Camera

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.otaliastudios.cameraview.CameraListener
import com.wonderkiln.camerakit.*
import kotlinx.android.synthetic.main.camera_share_fragment.*
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class CameraShareFragment : Fragment() {

    companion object {
        fun newInstance() = CameraShareFragment()
    }

    private lateinit var viewModel: CameraShareViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(ir.hamedrajabpour.fish.R.layout.camera_share_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CameraShareViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ShareGallery_Camera.addCameraKitListener(object : CameraListener(), CameraKitEventListener {
            override fun onVideo(p0: CameraKitVideo?) {}

            override fun onEvent(p0: CameraKitEvent?) {}

            override fun onError(p0: CameraKitError?) {}

            override fun onImage(p0: CameraKitImage?) {
                val jpeg = p0?.jpeg
                val result = BitmapFactory.decodeByteArray(jpeg, 0, jpeg!!.size)
                if (isExternalStorageWritable()) {
                    saveImage(result)
                }else{
                    //prompt the user or do something
                }
            }
        })

        ShareGallery_Camera_Button.setOnClickListener { ShareGallery_Camera.captureImage() }

//        ShareVideoChangeCamera.setOnClickListener { ShareGallery_Camera.toggleFacing() }


    }


    override fun onResume() {
        super.onResume()
        ShareGallery_Camera.start()
    }

    override fun onPause() {
        ShareGallery_Camera.stop()
        super.onPause()
    }


    @SuppressLint("SimpleDateFormat")
    private fun saveImage(finalBitmap: Bitmap) {

        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/saved_images")
        myDir.mkdirs()

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val fname = "Shutta_$timeStamp.jpg"

        val file = File(myDir, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /* Checks if external storage is available for read and write */
    fun isExternalStorageWritable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }




}


