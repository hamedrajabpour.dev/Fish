package ir.hamedrajabpour.fish.Share.Gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ir.hamedrajabpour.fish.R

class GalleryShareFragment : Fragment() {

    companion object {
        fun newInstance() = GalleryShareFragment()
    }

    private lateinit var viewModel: GalleryShareViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gallery_share_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GalleryShareViewModel::class.java)
        // TODO: Use the ViewModel

    }

}
