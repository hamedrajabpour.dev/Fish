package ir.hamedrajabpour.fish

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import ir.hamedrajabpour.fish.Share.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        BottomNavigation.setupWithNavController(navController)

//        NavigationUI.setupActionBarWithNavController(this,navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.shareFragment -> {
                    hideBottomNavigation()
                }
                R.id.galleryShareFragment -> hideBottomNavigation()

                R.id.cameraShareFragment -> hideBottomNavigation()

                R.id.videoShareFragment -> hideBottomNavigation()

                R.id.editProfileFragment -> hideBottomNavigation()
                else -> showBottomNavigation()
            }
        }

        BottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.shareFragment -> {
                    val bottomsheet = BottomSheetDialog()
                    bottomsheet.show(supportFragmentManager, "BottomSheetDialog")
                }

            }
            return@setOnNavigationItemSelectedListener true

        }

    }


    private fun hideBottomNavigation() {
        with(BottomNavigation) {
            if (visibility == View.VISIBLE && alpha == 1f) {
                visibility = View.GONE
            }
        }
    }

    private fun showBottomNavigation() {
        with(BottomNavigation) {
            visibility = View.VISIBLE
        }

    }
}

